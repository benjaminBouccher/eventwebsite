const firebaseConfig = {
    apiKey: "AIzaSyAVdEq5e2UYms2Y6lV4-nCXCP05A2-epNg",
    authDomain: "eventwebsite-6ad80.firebaseapp.com",
    projectId: "eventwebsite-6ad80",
    storageBucket: "eventwebsite-6ad80.appspot.com",
    messagingSenderId: "501413652801",
    appId: "1:501413652801:web:b6f9732311fddfb62419c0",
    measurementId: "G-PHCRLDDX71"
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let db = firebase.firestore();


function signUp() {
    let email = document.getElementById("email").value;
    let surname = document.getElementById("username").value;
    let passwordFirst = document.getElementById("passwordFirst").value;
    let passwordSecond = document.getElementById("passwordSecond").value;
    if (passwordFirst == passwordSecond) {
        firebase.auth().createUserWithEmailAndPassword(email, passwordFirst).then(() => {
            db.collection("userProfile").doc(firebase.auth().currentUser.uid).set({
                username: surname
            });
            window.location.href = "src.html";
            window.alert("Account created!");
        }).catch((e) => {
            window.alert("Error thrown: " + e.message + ". The account creation failed.");
        });
        document.getElementById("email").value = "";
        document.getElementById("username").value = "";
        document.getElementById("passwordFirst").value = "";
        document.getElementById("passwordSecond").value = "";
    } else {
        window.alert("The passwords do not match.");
    }
}

function logInFromSignUp() {
    window.location.href = "signuppage.html";
}


function logIn() {
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    document.getElementById("password").value = "";
    firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            activeUser = userCredential.user;
            window.location.href = "src.html";
            window.localStorage.setItem('username',email);
        })
        .catch((error) => {
            window.alert(error.message);
        });
}

function signUpFromLogIn() {
    window.location.href = "loginpage.html";
}


function logOut() {
    window.location.href = "signuppage.html";
    firebase.auth().signOut().then(function () {
        // Sign-out successful.
    }, function (error) {
        // An error happened.
    });
}