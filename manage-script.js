const firebaseConfig = {
    apiKey: "AIzaSyAVdEq5e2UYms2Y6lV4-nCXCP05A2-epNg",
    authDomain: "eventwebsite-6ad80.firebaseapp.com",
    projectId: "eventwebsite-6ad80",
    storageBucket: "eventwebsite-6ad80.appspot.com",
    messagingSenderId: "501413652801",
    appId: "1:501413652801:web:b6f9732311fddfb62419c0",
    measurementId: "G-PHCRLDDX71"
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let db = firebase.firestore();

function openModal() {
    let modal = document.getElementById("myModal");
    modal.style.display = "block";
}

function closeModal() {
    let modal = document.getElementById("myModal");;
    modal.style.display = "none";
}

let selectedEvent = { id: 0 };

function openEditModal(id) {
    let modal = document.getElementById("editModal");
    selectedEvent.id = id;
    db.collection('events').doc(id).get().then((doc) => {
        console.log(doc.data());
        document.getElementById("eventNameEdit").value = doc.data().name;
        document.getElementById("eventDateEdit").value = doc.data().date;
        document.getElementById("eventTimeEdit").value = doc.data().time;
        document.getElementById("maxParticipantEdit").value = doc.data().maxParticipant;
        document.getElementById("eventDescriptionEdit").value = doc.data().description;
        document.getElementById("addressEdit").value = doc.data().address;
    });
    modal.style.display = "block";
}

function closeEditModal() {
    let modal = document.getElementById("editModal");
    modal.style.display = "none";
}

function openConfirmation() {
    var eventName = document.getElementById("eventName").value;
    var eventDate = document.getElementById("eventDate").value;
    var eventTime = document.getElementById("eventTime").value;
    var maxParticipant = document.getElementById("maxParticipant").value;
    var eventDescription = document.getElementById("eventDescription").value;
    var eventAddress = document.getElementById("address").value;
    if (!isNaN(maxParticipant)) {
        db.collection("userProfile").doc(firebase.auth().currentUser.uid).get().then((doc) => {
            db.collection("events").add({
                date: eventDate,
                description: eventDescription,
                maxParticipant: maxParticipant,
                name: eventName,
                time: eventTime,
                address: eventAddress,
                participantList: {},
                creator: localStorage.getItem('username'),
            });
        });
        document.getElementById("eventName").value = "";
        document.getElementById("eventDate").value = "";
        document.getElementById("eventTime").value = "";
        document.getElementById("maxParticipant").value = "";
        document.getElementById("eventDescription").value = "";
        document.getElementById("address").value = "";
        closeModal();
        let modal = document.getElementById("confirmation");
        modal.style.display = "block";
    }
}

function openConfirmationEdit() {
    var eventName = document.getElementById("eventNameEdit").value;
    var eventDate = document.getElementById("eventDateEdit").value;
    var eventTime = document.getElementById("eventTimeEdit").value;
    var maxParticipant = document.getElementById("maxParticipantEdit").value;
    var eventDescription = document.getElementById("eventDescriptionEdit").value;
    var eventAddress = document.getElementById("addressEdit").value;
    db.collection("events").doc(selectedEvent.id).update({
        date: eventDate,
        maxParticipant: maxParticipant,
        time: eventTime,
        description: eventDescription,
        name: eventName,
        address: eventAddress,
    });
    document.getElementById("eventNameEdit").value = "";
    document.getElementById("eventDateEdit").value = "";
    document.getElementById("eventTimeEdit").value = "";
    document.getElementById("maxParticipantEdit").value = "";
    document.getElementById("eventDescriptionEdit").value = "";
    document.getElementById("addressEdit").value ="";
    closeEditModal();
    let modal = document.getElementById("confirmation");
    modal.style.display = "block";
}

function closeConfirmation() {
    let modal = document.getElementById("confirmation");
    modal.style.display = "none";
}

function fillEvents() {
    db.collection("events").get().then((result) => {
        result.forEach((doc) => {
            if (doc.data().creator == localStorage.getItem("username")) {
                displayEvents({ data: doc.data(), id: doc.id });
            }
        });
    });
}

function logOut() {
    window.location.href = "signuppage.html";
    firebase.auth().signOut().then(function () {
        // Sign-out successful.
    }, function (error) {
        // An error happened.
    });
}

function displayEvents(jsonObject) {
    let date = jsonObject.data.date;
    let time = jsonObject.data.time;
    let name = jsonObject.data.name;
    let description = jsonObject.data.description;
    console.log(description);
    let containerDiv = document.createElement('div');
    containerDiv.style.marginLeft = "100px";
    containerDiv.className = "display-job";
    containerDiv.addEventListener("click", () => {
        openEditModal(jsonObject.id);
    });

    let displayDiv = document.createElement('div');
    displayDiv.className = "displayDescriptionPlusIndex";

    let nameDiv = document.createElement('div');
    let nameItem = document.createElement('h4');
    nameItem.style.padding = "10px";
    nameItem.style.margin = "0px";
    nameItem.style.fontSize = "20px";
    nameItem.style.fontWeight = "bold";
    nameItem.textContent = name;
    nameDiv.appendChild(nameItem);

    let dateDiv = document.createElement('div');
    let dateItem = document.createElement('p');
    dateItem.textContent = date;
    dateItem.style.padding = "10px";
    dateItem.style.margin = "0px";
    dateItem.style.fontWeight = "bold";

    let timeItem = document.createElement('p');
    timeItem.textContent = time;
    timeItem.style.color = "#BD4257";
    timeItem.style.fontSize = "16px";
    timeItem.style.padding = "10px";
    timeItem.style.margin = "0px";
    timeItem.style.fontWeight = "bold";
    dateDiv.appendChild(dateItem);
    dateDiv.appendChild(timeItem);

    let descriptionItem = document.createElement('p');
    descriptionItem.textContent = description;

    displayDiv.appendChild(nameDiv);
    displayDiv.appendChild(dateDiv);
    displayDiv.appendChild(descriptionItem);
    containerDiv.appendChild(displayDiv)
    let fullContainer = document.getElementById('listEvents');
    fullContainer.appendChild(containerDiv);
}
