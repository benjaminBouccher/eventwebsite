const firebaseConfig = {
    apiKey: "AIzaSyAVdEq5e2UYms2Y6lV4-nCXCP05A2-epNg",
    authDomain: "eventwebsite-6ad80.firebaseapp.com",
    projectId: "eventwebsite-6ad80",
    storageBucket: "eventwebsite-6ad80.appspot.com",
    messagingSenderId: "501413652801",
    appId: "1:501413652801:web:b6f9732311fddfb62419c0",
    measurementId: "G-PHCRLDDX71"
};
  

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let db = firebase.firestore();

let array = [];

function getEvents(){
    array = [];
    db.collection("events")
    .get()
    .then((result) => {
      result.forEach((doc) => {
        console.log(doc.data());
        array.push(doc.data());
        displayEvents({data: doc.data(), id: doc.id})
    });
    });
}

objectElement = {};
function displayEvents(jsonObject){
    let date = jsonObject.data.date;
    let time = jsonObject.data.time;
    let name = jsonObject.data.name;
    let description = jsonObject.data.description;
    let containerDiv = document.createElement('div');
    containerDiv.className = "display-job";
    containerDiv.style.width = "40%";

    let displayDiv = document.createElement('div');

    containerDiv.addEventListener("click", () => {
        openInfoModal(jsonObject.id);
        objectElement = jsonObject;
    });

    let nameDiv = document.createElement('div');
    let nameItem = document.createElement('h4');
    nameItem.classList.add("eventName");
    nameItem.textContent = name;
    nameDiv.appendChild(nameItem);

    let dateDiv = document.createElement('div');
    let dateItem = document.createElement('p');
    dateItem.classList.add("eventDate");
    dateItem.textContent = date + " - " + time;
    dateDiv.appendChild(dateItem);

    let descriptionItem = document.createElement('p');
    descriptionItem.textContent = description;
    displayDiv.appendChild(nameDiv);
    displayDiv.appendChild(dateDiv);
    displayDiv.appendChild(descriptionItem);
    containerDiv.appendChild(displayDiv);
    let fullContainer = document.getElementById('newEvents');
    fullContainer.appendChild(containerDiv);
}


function addParticipant(){
    let userList = [];
    userList = objectElement.data.participantList;
    userList.push(localStorage.getItem('username'));
    db.collection("events").doc(objectElement.id).update({
        date: objectElement.data.date,
        maxParticipant: objectElement.data.maxParticipant,
        time: objectElement.data.time,
        description: objectElement.data.description,
        name: objectElement.data.name,
        participantList: userList,
        address: objectElement.address,
    });
}

let selectedEvent = { id: 0 };

function openInfoModal(id){
    let modal = document.getElementById("infoModal");
    selectedEvent.id = id;
    db.collection('events').doc(id).get().then((doc) => {
        console.log(doc.data());
        document.getElementById("eventNameEdit").textContent = doc.data().name;
        document.getElementById("eventDateEdit").textContent = doc.data().date;
        document.getElementById("eventTimeEdit").textContent = doc.data().time;
        document.getElementById("maxParticipantEdit").textContent = doc.data().maxParticipant;
        document.getElementById("eventDescriptionEdit").textContent = doc.data().description;
        document.getElementById("addressEdit").textContent = doc.data().address;
    });
    modal.style.display = "block";
}

function closeInfoModal(){
    let modal = document.getElementById("infoModal");
    modal.style.display = "none";
}


function logOut() {
    window.location.href = "signuppage.html";
    firebase.auth().signOut().then(function () {
        // Sign-out successful.
    }, function (error) {
        // An error happened.
    });
}